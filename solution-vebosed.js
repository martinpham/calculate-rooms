

const min = 3
const max = 10
let rows = Math.floor(Math.random() * max) + min
let columns = Math.floor(Math.random() * max) + min

let data = [
]

for(let r = 0; r < rows; r++)
{
	data[r] = []
	for(let c = 0; c < columns; c++)
	{
		data[r][c] = (Math.floor(Math.random() * 100) % 2) ? 1 : 0
	}
}


for(let r = 0; r < rows; r++)
{
	console.log(
		data[r].join(" ")
		.replace(/0/g, " ").replace(/1/g, "*")
	)
}

console.log()
console.log(rows + " x " + columns)
console.log()


Array.prototype.hasItem = function(child) {
	return this.findIndex(item => (item[0] === child[0] && item[1] === child[1])) > -1
} 

Array.prototype.hasRoomWhichHasChild = function(child) {
	return this.findIndex(room => room.hasItem(child)) > -1
} 



const expand = (r, c, room) => {
	console.log('>> Expanding',  r, c)
	if(r > 0)
	{
		if(room.hasItem([r - 1, c]))
		{
			console.log('>> up: Already checked',  r - 1, c, ' -> halt')
		} else {
			console.log('>> r > 0 -> check up',  r - 1, c)
			if(data[r - 1][c] === 1)
			{
				console.log('>>> Good',  r - 1, c)
				
				if(!room.hasItem([r - 1, c]))
				{
					room.push([r - 1, c])
					expand(r - 1, c, room)
				}
			}
		}
	}
	
	
	if(r < rows - 1)
	{
		if(room.hasItem([r + 1, c]))
		{
			console.log('>> down: Already checked',  r + 1, c, ' -> halt')
		} else {
			console.log('>> r <' , rows - 1, ' -> check down',  r + 1, c)
			if(data[r + 1][c] === 1)
			{
				console.log('>>> Good',  r + 1, c)
				
				if(!room.hasItem([r + 1, c]))
				{
					room.push([r + 1, c])
					expand(r + 1, c, room)
				}
			}
		}
	}
	
	if(c > 0)
	{
		if(room.hasItem([r, c - 1]))
		{
			console.log('>> left: Already checked',  r, c - 1, ' -> halt')
		} else {
			console.log('>> c > 0 -> check left',  r, c - 1)
			if(data[r][c - 1] === 1)
			{
				console.log('>>> Good',  r, c - 1)
				
				if(!room.hasItem([r, c - 1]))
				{
					room.push([r, c - 1])
					expand(r, c - 1, room)
				}
			}
		}
	}
	
	
	if(c < columns - 1)
	{
		if(room.hasItem([r, c + 1]))
		{
			console.log('>> right: Already checked',  r, c + 1, ' -> halt')
		} else {
			console.log('>> c <' , columns - 1, ' -> check right',  r, c + 1)
			if(data[r][c + 1] === 1)
			{
				console.log('>>> Good',  r, c + 1)
				
				if(!room.hasItem([r, c + 1]))
				{
					room.push([r, c + 1])
					expand(r, c + 1, room)
				}
			}
		}
	}
	
	console.log('>> Done expanding',  r, c)
	return room
}

const count = () => {
	const rooms = []
	
	for(let r = 0; r < rows; r++)
	{
		for(let c = 0; c < columns; c++)
		{
			console.log()
			console.log('Check', r, c)
			
			if(rooms.hasRoomWhichHasChild([r, c]))
			{
				console.log('Already exists a room has ', r, c)
			} else {
				if(data[r][c] === 1)
				{
					console.log('> Found', r, c)
					
					const expanded = expand(r, c, [
										[r, c]
									])
									
					console.log('> Expanded', expanded)
					
					rooms.push(expanded)
					
					console.log()
				}
			}
		}
	}
	
	return rooms
}

const rooms = count()
console.log('Total rooms', rooms.length)
