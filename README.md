**Problems**

Giving a floor with tiles

```
X 0 0 X
0 X 0 0
0 X 0 0
```

Where X is free space, 0 is wall space

A room can be composed by connected free spaces, rooms are divided by wall

Try to calculate how many rooms can be composed there

In above example, there are 3 rooms

```
X - - -
- - - -
- - - -
```
```
- - - X
- - - -
- - - -
```
```
- - - -
- X - -
- X - -
```

--


**Input**:

A 2D matrix, where 1 is free space, 0 is wall space

```
[
	[1, 0, 0, 1],
	[0, 1, 0, 0],
	[0, 1, 0, 0],
]
```
**Output**:

Number of rooms can be composed

```
3
```
