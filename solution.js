const roomHasItem = (room, item) => room.findIndex(child => (item[0] === child[0] && item[1] === child[1])) > -1;
const existsRoomHasItem = (rooms, item) => rooms.findIndex(room => roomHasItem(room, item)) > -1;
const checkItem = (r, c, room) => !roomHasItem(room, [r, c]) && data[r][c] === 1 && !roomHasItem(room, [r, c]) && room.push([r, c]);
const expandCheck = (data, r, c, room) => {
	(r > 0) && checkItem(r - 1, c, room) && expandCheck(data, r - 1, c, room);
	(r < data.length - 1) && checkItem(r + 1, c, room) && expandCheck(data, r + 1, c, room);
	(c > 0) && checkItem(r, c - 1, room) && expandCheck(data, r, c - 1, room);
	(c < data[0].length - 1) && checkItem(r, c + 1, room) && expandCheck(data, r, c + 1, room);
	return room
}
const rooms = []
for(let r = 0; r < data.length; r++) for(let c = 0; c < data[0].length; c++) !existsRoomHasItem(rooms, [r, c]) && data[r][c] === 1 && rooms.push(expandCheck(data, r, c, [[r, c]]))
console.log('Total rooms = ' + rooms.length)
